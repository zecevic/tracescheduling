package scheduling;

import java.util.List;

/**
 * Created by Darinka Zecevic on 7/15/2017.
 */
public class Resource {

    private List<Operation.Type> supported_types;
    private int count;
    private int cycle_num;

    public Resource(List<Operation.Type> supported_types, int count, int cycle_num) {
        this.supported_types = supported_types;
        this.count = count;
        this.cycle_num = cycle_num;
    }

    public boolean supports(Operation.Type type){
        return supported_types.contains(type);
    }

    public static void set_free_resource(List<Resource> resources, Operation.Type type){
        for (int i = 0; i<resources.size(); i++){
            if (resources.get(i).supports(type)){
                resources.get(i).setCount(resources.get(i).getCount()+1);
                break;
            }
        }
    }
    public static boolean check_available(List<Resource> resources, Operation.Type type){
        for (int i = 0; i<resources.size(); i++){
            if (resources.get(i).supports(type) && resources.get(i).getCount()>0)
               return true;
        }
        return false;
    }

    public static void set_busy_resource(List<Resource> resources, Operation.Type type){
        for (int i = 0; i<resources.size(); i++){
            if (resources.get(i).supports(type)){
                resources.get(i).setCount(resources.get(i).getCount()-1);
                break;
            }
        }
    }
    public List<Operation.Type> getSupported_types() {
        return supported_types;
    }

    public void setSupported_types(List<Operation.Type> supported_types) {
        this.supported_types = supported_types;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCycle_num() {
        return cycle_num;
    }

    public void setCycle_num(int cycle_num) {
        this.cycle_num = cycle_num;
    }
}
