package scheduling;

import java.util.Objects;

/**
 * Created by Darinka Zecevic on 7/15/2017.
 */
public class Operation {

    public enum Type {

        ADD, SUB, MUL, DIV
    }

    private Type type;
    private String first_operand, second_operand, result;

    public Operation() {}

    public String toString(){

        StringBuilder sb = new StringBuilder();
        sb.append(result);
        sb.append("=");
        sb.append(first_operand);
        switch (type){

            case ADD: sb.append("+"); break;
            case SUB: sb.append("-"); break;
            case DIV: sb.append("/"); break;
            case MUL: sb.append("*"); break;
        }
        sb.append(second_operand);
        return sb.toString();

    }


    @Override
    public boolean equals(Object object) {
        if (object instanceof Operation){
            Operation operation = (Operation) object;
            return (operation.type == type && operation.first_operand.equals(first_operand) &&
                    operation.second_operand == second_operand && operation.result == result);
        }else {
            return false;
        }
    }

    public Operation(Type type, String result, String first_operand, String second_operand) {
        this.type = type;
        this.first_operand = first_operand;
        this.second_operand = second_operand;
        this.result = result;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getFirst_operand() {
        return first_operand;
    }

    public void setFirst_operand(String first_operand) {
        this.first_operand = first_operand;
    }

    public String getSecond_operand() {
        return second_operand;
    }

    public void setSecond_operand(String second_operand) {
        this.second_operand = second_operand;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
