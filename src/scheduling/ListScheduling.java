package scheduling;

/**
 * Created by Darinka Zecevic on 7/15/2017.
 */

import java.util.ArrayList;
import java.util.List;

public class ListScheduling {

    private List<Operation> operations;
    private List<Resource> resources;
    private List<List<Operation>> result_schedule;

    private boolean[] scheduled_operations;
    int[][] dependency_graph;
    public ListScheduling(){}

    public ListScheduling(List<Operation> operations, List<Resource> resources) {
        this.operations = operations;
        this.resources = resources;
        scheduled_operations = new boolean[operations.size()];
        for (int i = 0; i<scheduled_operations.length; i++)
            scheduled_operations[i] = false;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    public List<List<Operation>> getResult_schedule() {
        return result_schedule;
    }

    private List<Operation> calculate_data_ready(){

        List<Operation> data_ready_operations = new ArrayList<>();
        //initial data_ready
        int size = operations.size();
        for (int i = 0; i<size; i++) {
            if (scheduled_operations[i])
                continue;

            boolean ready = true;
            for (int j = 0; j < size ; j++) {
                ready &= (dependency_graph[j][i] == 0);
            }

            if (ready)
                data_ready_operations.add(operations.get(i));
        }

        return data_ready_operations;
    }
    private String rename_variable(String variable){

        String index = "";
        int size = variable.length();
        for (int i = size -1; i>=0; i--){
            char c = variable.charAt(i);
            if (c >='0' && c<='9')
                index = c + index;
            else
                break;
        }
        if (index.isEmpty())
            return variable +"1";
        else {
            variable = variable.substring(0, variable.indexOf(index));
            int d = Integer.parseInt(index);
            d++;
            return variable+d;
        }
    }

    public void eliminate_antidependency(){
        // example :    read-write dependency  e = c + d
        //              c = a + b
        // rename
        int size = operations.size();
        for (int i = 0; i<size; i++){
            String first_operand = operations.get(i).getFirst_operand();
            String second_operand = operations.get(i).getSecond_operand();

            for (int j = i+1; j<size; j++){
                String result = operations.get(j).getResult();

                if (result.equals(first_operand) || result.equals(second_operand)){
                    //anti dependency
                    String new_result = rename_variable(result);
                    operations.get(j).setResult(new_result);
                    for (int k = j; k<size; k++){
                        if (result.equals(operations.get(k).getFirst_operand()))
                            operations.get(k).setFirst_operand(new_result);
                        if (result.equals(operations.get(k).getSecond_operand()))
                            operations.get(k).setSecond_operand(new_result);
                        if (result.equals(operations.get(k).getResult()))
                            operations.get(k).setResult(new_result);
                    }

                }


            }

        }

    }

    public void eliminate_outdependency(){

        // example : write-write dependency
        int size = operations.size();
        for (int i = 0; i<size; i++){

            String first_result = operations.get(i).getResult();
            for (int j = i+1; j<size; j++){

                String second_result = operations.get(j).getResult();

                if (first_result.equals(second_result)){

                    String new_result = rename_variable(second_result);

                    for (int k = j; k<size; k++){

                        if (second_result.equals(operations.get(k).getResult()))
                            operations.get(k).setResult(new_result);
                        if (second_result.equals(operations.get(k).getFirst_operand()))
                            operations.get(k).setFirst_operand(new_result);
                        if (second_result.equals(operations.get(k).getSecond_operand()))
                            operations.get(k).setSecond_operand(new_result);
                    }
                }
            }

        }

    }



    public int[][] make_dependency_graph(){


        int size = operations.size();
        int[][] dependency_matrix = new int[size][size];
        eliminate_antidependency();
        eliminate_outdependency();
        for (int i = 0; i<size; i++){
            String first_operand = operations.get(i).getFirst_operand();
            String second_operand = operations.get(i).getSecond_operand();
            int[] neighbours = new int[size];
            for (int j = 0; j<size; j++)
                neighbours[j] = 0;
            for (int j = i-1; j>=0; j--) {

                String previous_result = operations.get(j).getResult();
                boolean found_transitive = false;
                //direct dependency condition
                if (previous_result.equals(first_operand) || previous_result.equals(second_operand)) {
                    for (int k = 0; k<i; k++){
                        //eliminate transitive dependency
                        if (neighbours[k]==1 && dependency_matrix[j][k] == 1)
                            found_transitive = true;
                    }
                    if (!found_transitive) {
                        dependency_matrix[j][i] = 1;
                        neighbours[j] = 1;
                    }
                }


            }

        }
        return dependency_matrix;
    }


    private void update_dependency_graph(Operation operation){
        int size = operations.size();
        for (int i = 0; i<size; i++){

            if (operations.get(i).equals(operation)){
                scheduled_operations[i] = true;
                for (int j = 0; j<size; j++)
                    dependency_graph[i][j] = 0;
                break;
            }
        }

    }
    public List<List<Operation>> schedule() throws Exception {

        dependency_graph = make_dependency_graph();
        int size = operations.size();
        int i = 0;
        int cycle_num = 0;
        result_schedule = new ArrayList<>();

        while (i<size){

            List<Operation> data_ready_set = calculate_data_ready();
            List<Operation> current_cycle = new ArrayList<>();
            boolean[] scheduled = new boolean[data_ready_set.size()];
            for (int j = 0; j<data_ready_set.size(); j++)
                scheduled[j] = false;

            int num_scheduled = 0;

            for (int j = 0; j< data_ready_set.size(); j++){
                Operation.Type type = data_ready_set.get(j).getType();
                if (Resource.check_available(resources, type)){

                    Resource.set_busy_resource(resources, type);
                    scheduled[j] = true;
                    num_scheduled++;
                    current_cycle.add(data_ready_set.get(j));

                }
            }

            for (int j = 0; j<num_scheduled; j++){
                for (int k = 0; k<scheduled.length; k++){
                    if (scheduled[k]){
                        scheduled[k] = false;
                        Resource.set_free_resource(resources, data_ready_set.get(k-j).getType());
                        data_ready_set.remove(k-j);
                        break;
                    }
                }
            }

            i+= num_scheduled;
            for (int j = 0; j<current_cycle.size(); j++){
                update_dependency_graph(current_cycle.get(j));
            }
            result_schedule.add(current_cycle);
            cycle_num++;
            if (num_scheduled == 0)
                throw new Exception("Not enough resources for this basic block");

        }
        return result_schedule;
    }


}
